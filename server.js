const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");

const app = express();

app.use(fileUpload());
app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "api",
});

db.connect();

//get versions list
app.get("/versions", (req, res) => {
  db.query("SELECT * FROM version", function (err, data, fields) {
    if (err) throw err;
    return res.send(data);
  });
});

//add new version
app.post("/versions", function (req, res) {
  const data = req.body;

  //const file = req.files.file;

  /*file.mv("/uploads/"+ file.name, function(err){
    if(error){
      console.log("No se pudo guardar el archivo");
    } else{
      console.log("Archivo guardado con exito");
    }
  });*/

  db.query("INSERT INTO version SET ?", req.body, function (
    error,
    results,
    fields
  ) {
    if (error) throw error;
    return res.send({ data: results });
  });
});

app.listen(5000, () => console.log("Server Started..."));
