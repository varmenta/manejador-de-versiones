const express = require('express');
const router = express.Router();

//Create new version
router.post('/new', (req,res) => {
    let sql = `INSERT INTO version(version, description, date) VALUES (?)`;
    let values = [
        req.body.version,
        req.body.description,
        req.body.date
    ];
    db.query(sql, [values], function(err, data, fields) {
        if(err) throw err;
        res.json({
            status: 200,
            message: 'New version added successfully'
        })
    })
});

module.exports = router;