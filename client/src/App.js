import React from "react";
import FileUpload from "./pages/FileUpload";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import { View } from "./pages/View";

const App = () => (
  <div>
    <Router>
      <Navbar />
      <div className="container content">
        <Switch>
          <Route path="/" exact component={View} />
          <Route path="/registro" component={FileUpload} />
        </Switch>
      </div>
    </Router>
  </div>
);

export default App;
