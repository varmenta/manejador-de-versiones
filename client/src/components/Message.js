import React, { Fragment } from "react";
import PropTypes from "prop-types";

const Message = ({ msg }) => {
  return (
    <Fragment>
      <div
        className="alert alert-success alert-dismissible fade show"
        role="alert"
      >
        {msg}
        <button
          type="button"
          className="close"
          data-dismiss="alert"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </Fragment>
  );
};

Message.propTypes = {
  msg: PropTypes.string.isRequired,
};

export default Message;
