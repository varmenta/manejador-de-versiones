import React from "react";
import * as AiIcons from "react-icons/ai";

export const SidebarData = [
  {
    title: "View",
    path: "/",
    icon: <AiIcons.AiOutlineFile />,
    cName: "nav-text",
  },
  {
    title: "FileUpload",
    path: "/registro",
    icon: <AiIcons.AiFillFileAdd />,
    cName: "nav-text",
  },
];
