import React, { useState, Fragment } from "react";
import { useForm } from "react-hook-form";
import moment from "moment";
import Message from "../components/Message";

const FileUpload = () => {
  const { REACT_APP_URLBASE } = process.env;
  const [message, setMessage] = useState("");
  const { register, handleSubmit, reset, errors } = useForm();
  const date = {
    date: moment().format("LL"),
  };

  const onSubmit = async (data, e) => {
    const dataDate = Object.assign(data, date);
    const response = await fetch(`${REACT_APP_URLBASE}versions`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(dataDate),
    });
    console.log(await response.json());
    setMessage("Registro exitoso");
    e.target.reset();
  };

  return (
    <Fragment>
      {message ? <Message msg={message} /> : null}
      <form onSubmit={handleSubmit(onSubmit)} encType="multipart/form-data">
        <div className="form-group">
          <label>Versión</label>
          <input
            name="version"
            placeholder="Ingrese la versión del archivo"
            className="form-control"
            autoComplete="off"
            ref={register({ required: true })}
          />
          {errors.version && (
            <div className="alert alert-danger" role="alert">
              Debe ingresar la versión del proyecto.
            </div>
          )}
        </div>
        <div className="form-group">
          <label>Descripción</label>
          <textarea
            name="description"
            placeholder="Ingrese una descripción"
            className="form-control"
            ref={register({ required: true, maxLength: 100 })}
          />
          {errors.description && (
            <div className="alert alert-danger" role="alert">
              Debe ingresar la descripción del proyecto.
            </div>
          )}
        </div>
        <div className="form-group">
          <label>Seleccione un archivo</label>
          <input
            type="file"
            name="file"
            placeholder="Seleccione un archivo"
            id="inputGroupFile01"
            aria-describedby="inputGroupFileAddon01"
            className="form-control"
            ref={register({ required: true })}
          />
          {errors.file && (
            <div className="alert alert-danger" role="alert">
              Debe ingresar un archivo.
            </div>
          )}
        </div>
        <button type="submit" className="btn btn-primary">
          Registrar
        </button>
        <button type="button" className="btn btn-danger" onClick={reset}>
          Cancelar
        </button>
      </form>
    </Fragment>
  );
};

export default FileUpload;
