import React, { useEffect, useState } from "react";

export const View = () => {
  const [datos, setDatos] = useState([]);

  useEffect(() => {
    const { REACT_APP_URLBASE } = process.env;

    const getData = async () => {
      const data = await fetch(`${REACT_APP_URLBASE}versions`);
      const versions = await data.json();
      const versionsRevers = versions.reverse();
      setDatos(versionsRevers);
    };

    getData();
  }, []);

  return (
    <div className="row justify-content-md-center">
      {datos.map((item) => (
        <div
          className="card col-sm-3 mx-2 mt-3 shadow p-3 mb-5 bg-white rounded"
          style={{
            width: "19rem",
            display: "flex",
            flexDirection: "row",
          }}
          key={item.id}
        >
          <div className="card-body">
            <h5 className="card-title">Versión: {item.version} </h5>
            <h6 className="card-subtitle mb-2 text-muted">
              Fecha: {item.date}{" "}
            </h6>
            <p className="card-text">Descripción: {item.description}</p>
            <button type="button" className="btn btn-info">
              Descargar
            </button>
          </div>
        </div>
      ))}{" "}
    </div>
  );
};
